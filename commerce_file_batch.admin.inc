<?php
/**
 * @file
 * Administrative UI for commerce file batch
 */

/**
 * Implements hook_commerce_order_state_info().
 */
function commerce_file_batch_commerce_order_state_info() {
  $order_states = array();

  $order_states['batch_processing'] = array(
    'name' => 'batch_processing',
    'title' => t('Batch Processing'),
    'description' => t('Orders in this state are being re-processed to reflect updates on products.'),
    'weight' => 20,
    'default_status' => 'batch_processing_file_licenses',
  );

  return $order_states;
}

/**
 * Implements hook_commerce_order_status_info().
 */
function commerce_file_batch_commerce_order_status_info() {
  $order_statuses = array();

  $order_statuses['batch_processing_file_licenses'] = array(
    'name' => 'batch_processing_file_licenses',
    'title' => t('Issuing file licenses'),
    'state' => 'batch_processing',
    'cart' => TRUE,
  );

  return $order_statuses;
}


/**
 * Form builder function to allow choice of which products to batch process.
 */
function commerce_file_batch_file_license_issuing_form() {
  $products = commerce_product_load_multiple(array(), array('status' => 1), TRUE);
  $file_products = array('all' => 'All Products');
  foreach ($products as $product) {
    $file_products[$product->sku] = $product->title;
  }

  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => t('Issue file licenses by product or on all completed orders.'),
  );
  $form['products'] = array(
    '#type' => 'select',
    '#title' => 'Choose product(s)',
    '#multiple' => TRUE,
    '#size' => 20,
    '#options' => $file_products,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Process',
  );
  return $form;
}

function commerce_file_batch_file_license_issuing_form_submit($form, &$form_state) {

  $products = $form_state['values']['products'];

  $line_items = array();

  foreach ($products as $sku => $product) {
    if ($products['all'] == 'all') {
      $product_line_items = commerce_line_item_load_multiple(FALSE, array(), TRUE);
    }
    else {
      $product_line_items = commerce_line_item_load_multiple(array(), array('line_item_label' => $sku), TRUE);
    }
    foreach ($product_line_items as $product_line_item) {
      $line_items[] = $product_line_item;
    }
  }

  $count = count($line_items);
  $batch_limit = 50;
  $batch_count = ceil($count/$batch_limit);

  drupal_set_message(t('Processing @count line items', array('@count' => $count)));

  $operations = array();
  for ($i = 0; $i < $batch_count; $i++) {
    $operations[] = array('commerce_file_batch_issue_order_file_licenses', array(array_splice($line_items, NULL, $batch_limit)));
  }
  $batch = array(
    'operations' => $operations,
    'finished' => 'commerce_file_batch_finished',
  );
  batch_set($batch);
}

